/******************************************************************************
 * SamovArp
 * 
 * This souce code contains the complete implementation of a lightweight
 * arpeggiator which uses RawMidi of ALSA for IO and is written for Linux.
 * 
 * Author: Daniil Moerman
 **************************************/

/******************************************************************************
 * SECTION - Includes
 **************************************/

/* Includes from ANSI C */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>

/* Includes from Linux */
// TODO

/* Includes from ALSA */
#include <alsa/asoundlib.h> 


/******************************************************************************
 * SECTION - CLI Logging
 **************************************/

void 
smva_generic_log(
    const char* level,
    const char* fmt,
    va_list args
)
{
    assert(level != NULL);
    assert(fmt != NULL);

    printf("[%s] ", level);

    vprintf(fmt, args);
    va_end(args);
    printf("\n");
}

void 
smva_log_info(
    const char* fmt,
    ...
) {
    va_list args;
    va_start(args, fmt);
    smva_generic_log("I", fmt, args);
}

void 
smva_log_warning(
    const char* fmt,
    ...
) {
    va_list args;
    va_start(args, fmt);
    smva_generic_log("W", fmt, args);
}

void 
smva_log_error(
    const char* fmt,
    ...
) {
    va_list args;
    va_start(args, fmt);
    smva_generic_log("E", fmt, args);
}

/******************************************************************************
 * SECTION - ALSA RawMidi Client
 **************************************/

typedef struct {
    bool active;
    const char* port_name_in;
    const char* port_name_out;
    snd_rawmidi_t* alsa_handler_in; 
    snd_rawmidi_t* alsa_handler_out; 
} smva_alsa_rawmidi_client;

smva_alsa_rawmidi_client*
smva_alsa_rawmidi_client_create(void) {
    smva_alsa_rawmidi_client* obj=
        (smva_alsa_rawmidi_client*) malloc(sizeof(smva_alsa_rawmidi_client));
    obj->active = false;
    obj->port_name_in = NULL;
    obj->port_name_out = NULL;
    obj->alsa_handler_in = NULL;
    obj->alsa_handler_out = NULL;
    return obj;
}

void
smva_alsa_rawmidi_client_set_port_name_in(
    smva_alsa_rawmidi_client* obj,
    const char* port_name
) {
    assert(obj != NULL);
    assert(port_name != NULL);
    assert(obj->active != true);

    obj->port_name_in = (const char*) strdup(port_name);
}

void
smva_alsa_rawmidi_client_set_port_name_out(
    smva_alsa_rawmidi_client* obj,
    const char* port_name
) {
    assert(obj != NULL);
    assert(port_name != NULL);
    assert(obj->active != true);

    obj->port_name_out = (const char*) strdup(port_name);
}

void
smva_alsa_rawmidi_client_start(
    smva_alsa_rawmidi_client* obj
) {
    assert(obj != NULL);
    assert(obj->port_name_in != NULL);
    assert(obj->port_name_out != NULL);
    assert(obj->active != true);

    // Open ALSA connection (IN)
    int alsa_ret_val = 0;
    alsa_ret_val = snd_rawmidi_open(
        &obj->alsa_handler_in, 
        NULL, 
        obj->port_name_in, 
        SND_RAWMIDI_NONBLOCK
    );

    if(alsa_ret_val < 0) {
        obj->active = false;
        obj->alsa_handler_in = NULL;

        const char* alsa_error_msg = snd_strerror(alsa_ret_val);
        smva_log_error("Problem opening MIDI input: %s", alsa_error_msg);
    } else {
        obj->active = true;
        smva_log_info("Opened MIDI IN [%s]", obj->port_name_in);
    }

    // Open ALSA connection (OUT)
    alsa_ret_val = snd_rawmidi_open(
        NULL,
        &obj->alsa_handler_out, 
        obj->port_name_out, 
        SND_RAWMIDI_NONBLOCK
    );

    if(alsa_ret_val < 0) {
        obj->active = false;
        obj->alsa_handler_out = NULL;

        const char* alsa_error_msg = snd_strerror(alsa_ret_val);
        smva_log_error("Problem opening MIDI output: %s", alsa_error_msg);
    } else {
        obj->active = true;
        smva_log_info("Opened MIDI OUT [%s]", obj->port_name_out);
    }
}

void
smva_alsa_rawmidi_client_stop(
    smva_alsa_rawmidi_client* obj
) {
    assert(obj != NULL);
    assert(obj->active != false);
    assert(obj->alsa_handler_in != NULL);
    assert(obj->alsa_handler_out != NULL);
    
    snd_rawmidi_close(obj->alsa_handler_in);
    obj->alsa_handler_in = NULL;

    snd_rawmidi_close(obj->alsa_handler_out);
    obj->alsa_handler_out = NULL;

    obj->active = false;
}

bool
smva_alsa_rawmidi_client_is_active(
    smva_alsa_rawmidi_client* obj
) {
    assert(obj != NULL);
    return obj->active;
}

void
smva_alsa_rawmidi_client_destroy(
    smva_alsa_rawmidi_client** obj_ptr
) {
    assert(obj_ptr != NULL);
    assert((*obj_ptr) != NULL);

    smva_alsa_rawmidi_client* obj = (*obj_ptr);
    assert(obj->active != true);
    assert(obj->alsa_handler_in == NULL);
    assert(obj->alsa_handler_out == NULL);
    
    if(obj->port_name_in != NULL) {
        free((char*)obj->port_name_in);
        obj->port_name_in = NULL;
    }

    if(obj->port_name_out != NULL) {
        free((char*)obj->port_name_out);
        obj->port_name_out = NULL;
    }
    
    obj_ptr = NULL;
    
}

/******************************************************************************
 * SECTION - Entry point
 **************************************/

typedef struct {
    const char* rawmidi_in_port_name;
    const char* rawmidi_out_port_name;
    const char* message_queue_name;
    smva_alsa_rawmidi_client* rawmidi_client;
} smva_global_state;

smva_global_state global_state;

void
smva_runtime_parse_arguments(int argc, char *argv[]) {
    if(argc == 4) {
        global_state.rawmidi_in_port_name = argv[1];
        smva_log_info(
            "MIDI IN Port -> {%s}", 
            global_state.rawmidi_in_port_name
        );

        global_state.rawmidi_out_port_name = argv[2];
        smva_log_info(
            "MIDI OUT Port -> {%s}", 
            global_state.rawmidi_out_port_name
        );

        global_state.message_queue_name = argv[3];
        smva_log_info(
            "Message queue name -> {%s}", 
            global_state.message_queue_name
        );
    } else {
        smva_log_error("Always pass three arguments to me : MIDI_PORT_IN, MIDI_PORT_OUT, QUEUE_NAME");
    }
    return;
}

void
smva_runtime_open_midi_client(void) {
    global_state.rawmidi_client = smva_alsa_rawmidi_client_create();
    smva_alsa_rawmidi_client_set_port_name_in(
        global_state.rawmidi_client,
        global_state.rawmidi_in_port_name 
    );

    smva_alsa_rawmidi_client_set_port_name_out(
        global_state.rawmidi_client,
        global_state.rawmidi_out_port_name
    );


    smva_alsa_rawmidi_client_start(
        global_state.rawmidi_client
    );

    bool client_active =
        smva_alsa_rawmidi_client_is_active(
            global_state.rawmidi_client
        );

    if(client_active) {
        smva_log_info("RawMIDI Client client started!");
    } else {
        smva_log_error("RawMIDI client starting failed!");
        assert(client_active == true);
    }
}

void
smva_runtime_process_events(void) {
    return;
}

void
smva_runtime_close_midi_client(void) {
    smva_alsa_rawmidi_client_stop(
        global_state.rawmidi_client
    );
    
    smva_alsa_rawmidi_client_destroy(
        &global_state.rawmidi_client
    );
}

int main(int argc, char *argv[]) {
    smva_log_info("Parse arguments ...");
    smva_runtime_parse_arguments(argc, argv);

    assert(global_state.rawmidi_in_port_name != NULL);
    assert(global_state.rawmidi_out_port_name != NULL);
    assert(global_state.message_queue_name != NULL);

    smva_log_info("Open RawMIDI Client ...");
    smva_runtime_open_midi_client();

    smva_log_info("Process events ...");
    smva_runtime_process_events();

    smva_log_info("Close RawMIDI Client ...");
    smva_runtime_close_midi_client();
	return 0;
}