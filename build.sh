#!/bin/bash

# Clear & Create the target folder
rm -rf bin
mkdir bin

# Compile & Build the arpeggiator
gcc -g -o ./bin/samovarp -g ./application.c -lasound

# Make generator executable
chmod +x ./bin/samovarp
